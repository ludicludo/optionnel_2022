---
title: Programme et objectifs pédagogiques
sutitle: Optionnel DQPRM
author: 
  - ludovic.ferrer@ico.unicancer.fr
  - albertine.dubois@cea.fr
date: 13-14 juin 2022
---


| 13/6/2022                               | horaires   |
|-----------------------------------------|------------|
| **Accueil**                             |            |
| tour de table, binômage                 | 9:00-9:15  |
| introduction à la science reproductible | 9:15-9:30  |
| **git**                                 |            |
| Installation, prise en main             | 9:30-10:30 |

| 13/6/2022                       | horaires    |
|---------------------------------|-------------|
| **rappels ou décourvertes**     |             |
| python, bonnes pratiques        | 10:45-12:00 |
| **bibliothèques**               |             |
| numpy, pandas                   | 13:30-14:00 |
| simpleITK                       | 14:00-15:00 |
| Projet en binôme                | 15:00-17:00 |

| 14/06/2022                  | horaires    |
|-----------------------------|-------------|
| Projet suite                | 9:00-12:00  |
| Projet suite                | 14:00-16:30 |
| Rendu du projet/discussions | 16:30-17:00 |

