---
title: Découverte de git
sutitle: Optionnel DQPRM
author: 
  - ludovic.ferrer@ico.unicancer.fr
location: ICO René Gauducheau
date: 13-14 juin 2022
---
## Git
`git` est un outil qui permet de faire de la **gestion de version de fichiers**. 
Il a été développé par [Linus Torvald](https://fr.wikipedia.org/wiki/Linus_Torvalds)

:::{.question}
De quel autre développement informatique L.Torvald est-il le fondateur ?
:::


### Objectifs

- Mettre en place l'outil sur votre ordinateur (si pas déjà fait)
- Se familiariser avec les principaux concepts
- Utiliser **les principales commandes** pour utiliser ces concepts en mode solo
- Passer en mode multi-utilisateurs pour travailler sur un document partagé

### Concepts

`git` garde l'historique des fichiers comme une **séries de photos instantanés** d'un mini-système de fichiers.

![stockage des données du projet au fil du temps](https://git-scm.com/book/en/v2/images/snapshots.png)

La plupart des opérations se font sur l'**ordinateur local** où vous travaillez. Il n'est pas nécessaire de mettre en place de connexion avec un serveur. La totalité de l'historique des fichiers se trouve sur l'ordinateur local dans une base de données  appelée **dépôt local**.

Dans un fonctionnement standard, il est recommandé de créer **un dépôt local par projet**.

#### La sainte trinité: `modified, staged, commited`

Les fichiers peuvent être dans 3 états différents :

`modified`
 : le fichier est modifié mais n'est pas encore validé en base.

`staged` (indexé)
 : Le fichier est marqué comme modifié dans sa version actuelle pour qu'il fasse partie du prochain instantané en base.

`commited` (validé)
 : les données sont stockées en sécurité dans la base locale.
 
 Ces 3 états sont directement liés aux zones de travail, d'indexation et le répertoire git.
 
 ![](https://git-scm.com/book/en/v2/images/areas.png){width=25%}

 Le fonctionnement se schématise ainsi:

- **Modification des fichiers** dans le répertoire de travail
- **Indexation des fichiers modifiés**, ce qui ajoute des instantanés de ces fichiers dans la zone d'index
- **Validation** qui bascule les instantanés des fichiers de l'index dans la base de données du répertoire `git`.


Dans un fonctionnement un peu plus avancé, il sera intéressant de connaître la notion de **branche** qui permet de créer une **nouvelle version** des fichiers qui peuvent ainsi **évoluées en parallèle** avec ceux de la branche dont ils sont issus. Il existe des mécanismes pour fusionner les branches avec la branche principale.

### Installation

L'installation de `git` est détaillée [ici](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git) pour les systèmes d'exploitation couramment utilisés.

Au cours de cette session de travail, nous utiliserons exclusivement la ligne de commande. A noter que les outils d'édition de codes présentent couramment une interface avec `git`.

#### Premiers paramètrages

Dans le terminal, entrez la commande suivante en substituant les noms et adresses fictifs par vos propres identifiants.

```
$ git config --global user.name "Jean Bond"
$ git config --global user.email jean.bond@mi6.uk

```
Cela permet de rentrer vos coordonnées pour l'ensemble des projets sur l'ordinateur local. En cas d'utilisation partagé de l'ordinateur, il suffit d'enlever le paramètre `--global` pour un projet spécifique.


Dans ce même terminal, créer un répertoire de travail et déplacez y vous.

```
$ mkdir mon_repertoire
$ cd mon_repertoire
```
Lancer la commande `ls -a`

:::{.question}
- Le répertoire contient-il des fichiers ou des répertoires ?
:::

### Principales commandes

Les principales commandes sont :

| commandes | actions                                   |
|-----------|-------------------------------------------|
| help      | affiche l'aide en ligne                   |
| init      | initialise le répertoire `git`            |
| status    | détermine l'état des fichiers             |
| add       | ajoute les fichiers modifiés dans l'index |
| commit    | valide les fichiers indexés dans la base  |
| log       | affiche l'historique et les commentaires  |


#### `help`

La première des commandes :smile:

``` git help command```

Par exemple,

``` git help config```

Pour une aide moins détaillée, taper la commande: ```git command -h```

#### init/clone

Dans le répertoire de travail qui contient vos (futurs) fichiers, lancez la commande ```git init``` pour initialiser le répertoire `git`.

```
# c'est à vous de jouer
$ cd votre_repertoire_de_travail
$ git init
```
:::{.question}
- Comment s'appelle la branche initiale juste après l'initialisation
- Le répertoire contient-il des fichiers ou des répertoires ?
:::

#### status

Lancer la commande `git status`

```
$
```
:::{.question}
- Commenter ce que renvoit le terminal
:::

#### add

La commande `add` permet de suivre le(s) fichiers dans la zone d'indexation. Pour le moment, les modifications faites sur les fichiers mis dans cette zone ne sont pas encore enregistrées dans la base.

#### commit
L'enregistrement des modifications est réalisé par la commande `commit`. On doit associer un message pertinent à l'opération de sauvegarde en base.
Comme la sauvegarde est rapide et ne coute rien en terme de performance, il est recommandé de commiter souvent avec des messages qui correspondent aux modification atomiques faites sur les fichiers.

#### log

Cette commande permet de voir l'historique des modifications apportées aux fichiers.

<!-- <iframe width="525" height="295" src="https://www.youtube.com/embed/jUlT-zQ-mbk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

### Ressources internet

<iframe width="352" height="200" src="https://www.youtube.com/embed/HVsySz-h9r4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercice: mode solo

::::{.l-double}
:::{.two}
![](https://images.ladepeche.fr/api/v1/images/view/5c1230398fe56f390e7b9520/original/image.jpg)
:::
:::{.one}

Dans cet exercice, on demande de créer un répertoire qui contient un fichier texte qui contiendra les questions posées au-dessus et leurs réponses.

Avant de répondre aux questions, il faut d'abord créer un dépôt `git` dans le répertoire. 

On demande de créer un `commit` pour chaque couple question/réponse. Vous devriez donc executer la commande `commit` autant de fois qu'il y a de questions posées. Dans le mesure de votre inspiration, associer des commentaires pertinents aux réponses que vous validez (`commit`).
:::
::::

## Github, Gitlab, Bitbucket

`git` permet aussi de travailler à **plusieurs sur le même projet** en partageant le dépôt git sur **un serveur**. Plusieurs sociètés proposent ce type de service gratuitemment (`github`, `gitlab` ou  `bitbucket`) pour une petite équipe de développement.

### cloner un dépôt `git`

Sur des projets existants, il est possible de récupérer la totalité du répertoire `.git` et les fichiers de travail associés en utilisant la commande `clone`.

#### ssh ou https

Les transferts de données entre votre machine local et le serveur se font soit par le protocole d'échange `ssh` ou bien `https`.

La première méthode nécessite **un échange de clé ssh** entre chacun des ordinateurs où vous travaillerez (le poste local) et le serveur. La procédure pour mettre en place cet échange de clé est disponible pour les principaux fournisseurs de service [github](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account), [gitlab](https://docs.gitlab.com/ee/user/ssh.html) et [bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/).

La seconde méthode requière qu'à chaque envoi ou récupération de fichiers vers ou à partir du serveur, vous entriez un **identifiant** et **mot de passe** associé.

#### Exercice: ssh key

:::{.question}

- Choisir un serveur `git` distant i.e. `github`, `gitlab`, `bitbucket`
- Créer un compte gratuit sur le site du fournisseur de service `git` choisi
- Mettre en place un échange de clé ssh entre votre ordinateur local et le serveur `git` distant
:::

