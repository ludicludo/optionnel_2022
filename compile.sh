pandoc repro_science.md -t revealjs -f markdown+emoji -o repro_science_reveal.html -V theme=white --standalone -V revealjs-url=https://unpkg.com/reveal.js@4.0/ -c ./data/local.css --slide-level=4 -V history=false

pandoc repro_science.md -t Slidy -f markdown+emoji -o repro_science.html --standalone -c ./data/local_slidy.css --slide-level=2
