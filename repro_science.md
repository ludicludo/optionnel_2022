---
title: La démarche `science`&nbsp;`reproductible` \n en physique médicale
sutitle: Optionnel DQPRM
author: 
  - ludovic.ferrer@ico.unicancer.fr
location: ICO René Gauducheau
date: 13-14 juin 2022
---

## Contexte

### Situations comico-tragiques

![](http://www.phdcomics.com/comics/archive/phd052810s.gif)

####

![](http://www.phdcomics.com/comics/archive/phd031214s.gif)

![](http://www.phdcomics.com/comics/archive/phd032614s.gif)

####

![](http://www.phdcomics.com/comics/archive/phd101212s.gif){width=50%}

### Situations tragiques

#### Nombreuses rétractations scientiques
> However, while most scientists are careful to validate their laboratory and field equipment, most **do not know how reliable** their software is [4,5]. This can lead to **serious errors impacting the central conclusions of published research** [6]. Recent high profile retractations, technical comments and corrections because af errors in computational methods include papers in Science [7,8], PNAS [9], the journal of Molecular Biology [10] ...

:::{.citation}
Wilson et al - 2014. Best practices for Scientific Computing. Plos. Biol. 12:1
:::

#### Conséquences économiques

Reinhart-Rogoff ont publié en 2010 [Growth in a time of debt](https://scholar.harvard.edu/files/rogoff/files/growth_in_time_debt_aer.pdf) menant la Grèce a une politique d'austérité majeure.

![](https://www.researchgate.net/profile/Lauren-Talalay/publication/265902750/figure/fig1/AS:746156254572545@1554908922532/Cartoonist-Dave-Granlund-12-April-2010.ppm){width=33%}

Les résultats étaient erronées à cause d'**une mauvaise utilisation du tableur Excel**.

## Définition `science reproductible`

> **Replication**, the practice of independently implementing scientic experiments to validate specific findings, is the cornerstone of discovering scientic truth. Related to replication is **reproducibility**, which is the **calculation of quantitative scientific results** by **independent scientists** using **the original datasets and methods**. Reproducibility can be thought of as a different standard of validity because it forgoes independent data collection and uses the methods and data collected by the original investigator.

:::{.citation}
V. Stodden, F. Leisch, R. D. Peng, Implementing Reproducible science,
CRC Press, 14 avr. 2014 - 448 pages
:::

### Les bonnes pratiques

Quelques élèments de réflexions:

:::{#pistes}

- The Practice of Reproducible Research: Case Studies and Lessons from the Data-Intensive Sciences. Kitzes et al Oakland, CA: University of California Press, 2018
- Best Practices for Scientifc Computing, Wilson et al, Plos Biology, 2014
- Ten Simple Rules for Reproducible Computational Research, Sandve et al, PLoS Computational Biology, 2013

:::

### Quelques règles

- Utiliser un **cahier de notes**
- Eviter **les manipulations manuelles**
  - Les écrire dans le cahier de notes à défaut
- **Eviter les tableurs**
- **Automatiser** avec des langage de scripts
- Utiliser **un système de gestion de version**
- **Tester** le code et les données
- **Partager** le code et les données
- **Travailler à plusieurs**

:::{#labview}
![un cahier de notes intéractif](https://jupyter.org/assets/homepage/labpreview.webp)
:::

## En pratique

### Le cahier de notes/laboratoire{background-image="./imgs/notebook.png"}


Noter:

- La **date(/heure)** pour avoir un historique
- Toutes **modications manuelles** de fichiers numériques
- **Graines aléatoires** si traitements stochastiques
- Le type et les versions de votre environnent informatique
- Vos **réflexions** sous-tendues par vos résultats

Cahiers numériques: un simple fichier texte suffit.

Mise en forme simple,
: `markdown, rst, org`, ...

### Cahier de notes electronique

![Exemple de cahier de notes électronique](./imgs/orgmode.png){width=50%}

### Programmation lettrée

::::{.l-double}
:::{.one}
> Approche de **programmation** où le **code** sert à illustrer les sujets et points abordés dans la partie lettrée.

> Idéal pour la création de rapports ou de documentations techniques.

:::
:::{.two}
[![](./imgs/exemple_notebook.png){width=80%}](./imgs/exemple_notebook.png)
:::
::::

### Utiliser les scripts

- Interagir avec les données automatiquement
- Eviter les erreurs de recopie
- Automatisation de workflow des traitements
  - [kedro](https://kedro.readthedocs.io/en/stable/kedro.html), [snakemake](https://snakemake.github.io/), [airflow](https://airflow.apache.org/), ...

### Haro sur les manipulations avec les tableurs 


:thumbsup: Saisie manuelles de tableaux de données

:thumbsup: Un tableau **maximum** par feuille

:thumbsdown: ~~Altérer manuellement des données~~

:thumbsdown: Format propriétaire

:thumbsdown: Pas de sens linéaire de lecture

### Utiliser un gestionnaire de version

> Un gestionnaire de version est un système qui enregistre **l’évolution d’un fichier** ou d’un **ensemble de fichiers au cours du temps** de manière à ce qu’on puisse rappeler une **version antérieure** d’un fichier à tout moment.

:::{.citation}
[Pro git. 2^nd^ ed.](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-%C3%80-propos-de-la-gestion-de-version)
:::

::::::{.l-double}
:::::{.one}
:::{#small-git}
- Partage de données textuelles (codes, notebook, ...)
- Synchronisation entre plusieurs ordinateurs ou utilisateurs
- **Git**, mercurial, ...
- [github](https://www.github.com), [gitlab](https://www.gitlab.com), [bitbucket](https://bitbucket.org/), ...
  - services de **stockage en ligne à la git**
:::
:::::
:::::{.two}
::::{.l-double}
:::{.one}
[![](https://cbea.ms/content/images/size/w2000/2021/01/git_commit_2x.png){width=80%}](https://cbea.ms/content/images/size/w2000/2021/01/git_commit_2x.png)
:::
:::{.two}
[![](https://imgs.xkcd.com/comics/git.png){width=50%}](https://imgs.xkcd.com/comics/git.png)
:::
::::
:::::
::::::

### Partage des données et des codes

::::{.l-double}
:::{.one}
- On travaille mieux à plusieurs
- Documentation plus pertinentes
- Github, Gitlab, bitbucket 
:::
:::{.two}
![](./imgs/sharing.png){width=50%}
:::
::::

## Conclusion

La démarche `science reproductible` doit **améliorer la qualité des traitements informatiques des données** et le **savoir commun** par le partage des données et des codes d'analyses des données. 