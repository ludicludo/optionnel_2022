# coding: utf-8
import matplotlib.pyplot as plt



def show_one_by_two(coupe, img, label):
    '''show image and label side-by-side'''
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
    ax[0].imshow(img[coupe], cmap=plt.cm.gray)
    ax[1].imshow(label[coupe])
    plt.show()
